($(document).on("ready", function() {

    $("div[data-src],a[data-src]").each(function() {
        setElementBackground.apply(this);
    });

    function setElementBackground() {
        var elem = $(this);
        var src = elem.data("src");
        elem.css("background-image", `url(${src})`);
    }

    $(".switcher span").on("click", function() {
        var span = $(this);
        if (!span.hasClass("_selected")) {
            span.parent().find("span").removeClass("_selected");
            span.addClass("_selected");
            $(".services,.advantages").removeClass("_selected");
            $("." + span.data("toggle")).addClass("_selected");
        }
    })

    $(".news-item").mouseenter(function() {
        var news_item = $(this);
        var image = news_item.find(".news-item--image");
        var desc = news_item.find(".news-item--description");
        image.on("transitionend webkitTransitionEnd oTransitionEnd", function() {
            image.off("transitionend webkitTransitionEnd oTransitionEnd");
            if (!image.hasClass("_collapsed"))
                return;
            desc.slideDown(500);
        })
        image.addClass("_collapsed");

    })

    $(".news-item").mouseleave(function() {
        var news_item = $(this);
        var image = news_item.find(".news-item--image");
        var desc = news_item.find(".news-item--description");
        desc.slideUp(500, function() {
            image.removeClass("_collapsed");
        });
    })

    if ($("#light-slider").length != 0) {
        var slider = $("#light-slider").lightSlider({
            controls: false,
            auto: true,
            loop: true,
            pager: false,
            pause: 6000,
            responsive: [{
                breakpoint: 768,
                settings: {
                    item: 2
                }
            }, {
                breakpoint: 480,
                settings: {
                    item: 1
                }
            }]
        });

        $('.slidePrev').click(function() {
            slider.goToPrevSlide();
        });

        $('.slideNext').click(function() {
            slider.goToNextSlide();
        });
    }

    $(".service-item--title").mouseenter(function() {
        var title = $(this);
        var imageBlock = title.parent().parent().find(".service-item--image");
        var src = imageBlock.data("hover");
        imageBlock.css("background-image", `url(${src})`);
    })

    $(".service-item--title").mouseleave(function() {
        var title = $(this);
        var imageBlock = title.parent().parent().find(".service-item--image");
        var src = imageBlock.data("src");
        imageBlock.css("background-image", `url(${src})`);
    })

    $(".header--burger img").click(function() {
        $(".header--links").fadeToggle(300);
        if ($(this).attr("src").indexOf("opened") == -1) {
            $(this).attr("src", "img/burger_opened.png");
        } else {
            $(this).attr("src", "img/burger.png");
        }
    })

    $(window).resize(function() {
        var body = document.body;
        if (body.clientWidth > 767) {
            $(".header--burger img").attr("src", "img/burger.png");
            $(".header--links").css("display", "");
            $(".dropdown.open").removeClass("open");
        }

    })



}))
